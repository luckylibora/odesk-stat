package controllers

import helper.{Skill, ODesk}
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import scala.concurrent.ExecutionContext.Implicits.global

object Api extends Controller {

  implicit val skillFormat = Json.format[Skill]

  def oDesk = Action.async {
    ODesk.get.map(_.map(Skill.tupled).toSeq).map(t => Ok(Json.toJson(t)))
  }
}
