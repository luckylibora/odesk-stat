package controllers

import play.api.Routes
import play.api.cache.Cached
import play.api.mvc.{Action, Controller}
import play.api.Play.current

object JsRoutes extends Controller {

  def jsRoutes = Action {
    implicit request =>
      Ok(
        Routes.javascriptRouter("jsRoutes")(
          routes.javascript.Api.oDesk
        )
      ).as("text/javascript")
  }
}
