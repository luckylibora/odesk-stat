package helper

import org.jsoup.Jsoup
import play.api.Logger
import play.api.libs.ws.WS

import play.api.Play.current

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ODesk {

  val baseUrls = Seq[String](
    "https://www.odesk.com/o/jobs/browse/c/web-development/?page=",
    "https://www.odesk.com/o/jobs/browse/c/software-development/?page="
  )

  val pageCount = 100

  def url(baseUrl: String, index: Int) = baseUrl + index

  def getTags(url: String) = WS.url(url).get().map(r => {
    Logger.info("Parsed "+url)
    val doc = Jsoup.parse(r.body)
    doc.getElementsByClass("oSkill").map(e => e.text())
  })


  def get: Future[Map[String, Int]] = {
    val futureList = baseUrls.map(bu => 1.to(pageCount).map(url(bu, _)).map(getTags)).flatten
    Future.sequence(futureList).map(_.flatten.groupBy(s => s).mapValues(_.length))
  }

  def test = {
    getTags(url(baseUrls(0), 1))
  }
}
