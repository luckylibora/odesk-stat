import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import scala.collection.mutable.ArrayBuffer

import scala.language.implicitConversions

package object helper {

  implicit def elementsToSeq(e: Elements): ArrayBuffer[Element] = {
    val res = ArrayBuffer[Element]()
    val iterator = e.iterator()
    while (iterator.hasNext) {
      res += iterator.next()
    }
    res
  }

}
