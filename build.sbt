name := """odesk-api"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.jsoup" % "jsoup" % "1.8.1",
  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "org.webjars" % "requirejs" % "2.1.14-3",
  "org.webjars" % "requirejs-domready" % "2.0.1-2",
  "org.webjars" % "jquery" % "2.1.1",
  "org.webjars" % "angularjs" % "1.3.0",
  "org.webjars" % "bootstrap" % "3.2.0-2"
)

pipelineStages := Seq(rjs,uglify, digest, gzip)